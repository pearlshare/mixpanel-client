var got = require("got");
var url = require("url");

var BASEURL = "https://api.mixpanel.com/";

/*
 * Encode some data in base64
 * @param {Object} data - The data to encode
 * @returns {String} the encoded data
 */
function toB64 (data) {

  // Use a buffer so we don't have to use some external lib
  return (new Buffer(JSON.stringify(data))).toString("base64");
}


function MixpanelClient (apiToken, opts) {
  if (apiToken === undefined) {
    throw new Error("A token is required.");
  }
  this.apiToken = apiToken;

  // Set default options
  opts = opts || {};
  if (opts.userId) {
    this.userId = opts.userId;
  }

  if (opts.enabled === false) {
    this.enabled = false;
  } else {
    this.enabled = true;
  }

  if (opts.defaultTrackProperties) {
    this.defaultTrackProperties = opts.defaultTrackProperties
  }

  // Export the mixpanel url in use (usefull for CORS)
  this.url = BASEURL;

  return this;
}

/*
 * The function that makes requests to the API
 * @param {String} path - The path on the API to make the request to
 * @param {Object} data - The data to send with the request
 * @returns {Promise} promise resolving to a boolean if the request was successful
 */
MixpanelClient.prototype.makeRequest = function (path, data) {
  if (!this.enabled) {
    return Promise.resolve(true);
  }

  var reqUrl = url.resolve(this.url, path);

  return got(reqUrl, {
    method: "GET",
    query: {
      // Encode data as Base64 and add as query param (ewww)
      data: toB64(data)
    }
  }).then(function (res) {
    return res.body === "1"; // 1 - Success, 0 - Fail
  }).catch(function (e) {
    // Any error automatically means the request failed
    return false;
  });

}

/**
 * Trigger a mixpanel event for statistics (https://mixpanel.com/help/reference/http#tracking-via-http)
 * @param {String} eventType - The type of event to be recorded
 * @param {Object} otherData - Optional data to attach to the request
 * @returns {Promise} a promise resolving to if the request was successful
 */
MixpanelClient.prototype.track = function track (eventType, otherData) {
  otherData = otherData || {};

  var properties = Object.assign({
    "token": this.apiToken
  }, otherData);
  if (typeof(this.userId) !== "undefined") {
    properties.distinct_id = this.userId;
  }

  var data = {
    "event": eventType,
    "properties": properties
  }

  if (this.defaultTrackProperties) {
    data.properties = Object.assign(this.defaultTrackProperties, data.properties);
  }
  
  return this.makeRequest("track", data);
}

/**
 * Engage with a user on mixpanel (https://mixpanel.com/help/reference/http#people-analytics-updates)
 * @param {String} operationName    - The type of operation to perform on the user
 * @param {Object} userData         - Optional data to assign to the user
 * @param {Object} opts             - Engage options
 * @returns {Promise} a promise resolving to if the request was successful
 */
MixpanelClient.prototype.engage = function engage (operationName, userData, opts) {
  userData = userData || {};
  opts = opts || {};
  operationName = operationName.toLowerCase();

  // validations
  if (["$set", "$set_once", "$add", "$append", "$union", "$unset", "$delete"].indexOf(operationName) === -1) {
    return Promise.reject(new Error("UNKNOWN_OPERATION"));
  }
  if (typeof(this.userId) === "undefined") {
    return Promise.reject(new Error("USERID_REQUIRED"));
  }

  var data = {
    "$token": this.apiToken,
    "$distinct_id": this.userId
  };

  // Set the user ip address mixpanel uses to geolocate users
  if (opts.ip) {
    data["$ip"] = opts.ip;
  }

  // Set the time of the update, if not set mixpanel uses the time the request arrives
  if (opts.time) {
    data["$time"] = opts.time;
  }

  data[operationName] = userData;
  return this.makeRequest("engage", data);
}

/**
 * Set the mixpanel user to use when making requests
 * @param {String} userId   Attach a user to subsequent requests
 * @returns {undefined} no response
 */
MixpanelClient.prototype.identify = function identify (userId) {
  this.userId = userId;
}


module.exports = MixpanelClient;
