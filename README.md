# Mixpanel client

A basic wrapper to abstract the strangeness of the Mixpanel API and turn it into a small promise based client.

Mixpanel API weirdnesses include but are not limited to:

* base64 encoded query data
* "1" as success, "0" as error

## Usage

```js
var MixpanelClient = require("mixpanel-client");

// Create a client
var client = new MixpanelClient("my-token");

// Set the current user for all requests
client.identify("123");

// Trigger an event
client.track("myEvent", {
  extra: "data"
})
.then(function (response) {
  assert(response === true);
});

// Engage a user
client.engage("$set", {name: "Fish face"})
.then(function (response) {
  assert(response === true);
});
```

## Functions

** Mixpanel(apiToken, opts) **

Constructor which initializes the mixpanel client saving the apiToken and any given userId.

Options:

* enabled {boolean} - whether to make requests or not
* userId {String} - set the user (`distinct_id`) to make requests as
* defaultTrackProperties - add a set of default properties to track events. Useful for platform/locale information etc

** makeRequest(path, data) **

Make a request to mixpanel serializing any input data properly.

** identify(userId) **

Set the current mixpanel userId (`distinct_id`) sent when making the requests.

** track(eventName, data) **

Track an event on Mixpanel. This will use the token and any userId set.

** engage(operationName, data) **

Engage manages user profiles on Mixpanel. This will use the token and must have a userId set.
