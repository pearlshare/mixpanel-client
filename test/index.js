var expect = require("expect.js");
var url = require("url");
var qs = require("querystring");
var nock = require("nock");
var MixpanelClient = require("../");

describe("mixpanel-client", function () {

  afterEach(function (){
    nock.cleanAll();
  });


  describe("create", function () {
    it("Should export a function", function () {
      expect(MixpanelClient).to.be.a("function");
    });

    it("Should throw an error if a token is not provided", function () {
      expect(MixpanelClient).withArgs().to.throwError();
    });

    it("Should accept an apiToken without opts", function () {
      expect(MixpanelClient).withArgs("testtest123").to.not.throwError();
    });

    it("Should accept an apiToken with opts", function () {
      expect(MixpanelClient).withArgs("testtest123", {userId: "123123123"}).to.not.throwError();;
    });

    it("Should return an object after initialization", function () {
      var apiToken = "testtest123";
      var mixpanel = new MixpanelClient(apiToken, "123123123");

      expect(mixpanel).to.be.an("object");
      expect(mixpanel.apiToken).to.be.eql(apiToken);
    });
  });

  describe("track", function () {
    var eventType = "test";
    var opts = {userId: 31};
    var mixpanel = new MixpanelClient("asfjhasdfkjhgasdfkjhgasdf", opts);

    it("Should track a 'test' event", function () {
      nock("https://api.mixpanel.com")
      .get(function (uri) {
        var parsedUri = url.parse(uri);
        var data = JSON.parse((new Buffer(qs.parse(parsedUri.query).data, "base64")).toString());

        expect(parsedUri.pathname).to.be.eql("/track");

        expect(Object.keys(data)).to.contain("properties");

        expect(Object.keys(data.properties)).to.contain("token");
        expect(data.properties.distinct_id).to.be.eql(opts.userId);

        expect(data.event).to.be.eql(eventType);
        return true;
      })
      .once()
      .reply(200, "1");

      return mixpanel.track(eventType).then(function (res) {
        expect(res).to.eql(true);
      });
    });
  });

  describe("engage", function () {
    var mixpanel = new MixpanelClient("asfjhasdfkjhgasdfkjhgasdf", {userId: 33});


    it("Should store user profile", function () {
      var operationName = "$set";
      var profileData = {"Address": "1313 Mockingbird Lane"};

      nock("https://api.mixpanel.com")
      .get(function (uri) {
        var parsedUri = url.parse(uri);
        var data = JSON.parse((new Buffer(qs.parse(parsedUri.query).data, "base64")).toString());

        expect(parsedUri.pathname).to.be.eql("/engage");
        expect(Object.keys(data)).to.contain("$token");
        expect(Object.keys(data)).to.contain("$distinct_id");
        expect(Object.keys(data)).to.contain(operationName);
        expect(Object.keys(data)).not.to.contain("properties");
        expect((data)[operationName]).to.be.eql(profileData);
        return true;
      })
      .once()
      .reply(200, "1");

      return mixpanel.engage(operationName, profileData)
        .then(function (res) {
          expect(res).to.eql(true);
        });
    });

    it("Should reject on unknown operation", function () {
      var operationName = "$set66";
      var profileData = {"Address": "1313 Mockingbird Lane"};

      return mixpanel.engage(operationName, profileData)
        .then(function (res) {
          expect(res).to.be.a(Error);
        })
        .catch(function (err) {
          expect(err).to.be.a(Error);
          expect(err.message).to.match(/UNKNOWN_OPERATION/);
        });
    });

    it("Should reject on unknown userId", function () {
      var mixpanel2 = new MixpanelClient("asfjhasdfkjhgasdfkjhgasdf");
      var operationName = "$set";
      var profileData = {"Address": "1313 Mockingbird Lane"};

      return mixpanel2.engage(operationName, profileData)
        .then(function (res) {
          expect(res).to.be.a(Error);
        })
        .catch(function (err) {
          expect(err).to.be.a(Error);
          expect(err.message).to.match(/USERID_REQUIRED/);
        });
    });
  });

  describe("identify", function () {
    var mixpanel = new MixpanelClient("asfjhasdfkjhgasdfkjhgasdf");

    it("Should set the user", function () {
      var userId = "fishFace";
      mixpanel.identify(userId);

      expect(mixpanel.userId).to.equal(userId);
    });
  });

  describe("url", function () {
    var mixpanel = new MixpanelClient("asfjhasdfkjhgasdfkjhgasdf");

    it("should export a url", function () {
      expect(mixpanel.url).to.be.a("string")
    });
  });
});
